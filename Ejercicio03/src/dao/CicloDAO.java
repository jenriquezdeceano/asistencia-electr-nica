package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;






import conexion.Conexion;
import dto.CicloDTO;
import interfaces.Obligacion;

public class CicloDAO implements Obligacion<CicloDTO>{
	
	private static final String SQL_INSERT="insert into ciclo(nombre) VALUES (?)";
	private static final String SQL_DELETE="delete from ciclo where id=?";
	private static final String SQL_UPDATE="update from ciclo nombre=? where id=?";
	private static final String SQL_READ="select id,nombre from ciclo where id=?";
	private static final String SQL_READALL="select id,nombre from ciclo";
	
	private static final Conexion con = Conexion.saberEstado();

	@Override
	public boolean create(CicloDTO c) {
		PreparedStatement ps;
		try {
			
			ps = con.getCnn().prepareStatement(SQL_INSERT);
			ps.setString(1, c.getNombre());
			if(ps.executeUpdate()>0){
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			con.cerrarConexion();
		}

		return false;
	}

	@Override
	public boolean delete(Object key) {
		PreparedStatement ps;
		try {
			
			ps = con.getCnn().prepareStatement(SQL_DELETE);
			ps.setInt(1, key.hashCode());
			if(ps.executeUpdate()>0){
				return true;
				
			}
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			con.cerrarConexion();
		}

		return false;
	}

	@Override
	public boolean update(CicloDTO c) {
		PreparedStatement ps;
		try {
			
			ps = con.getCnn().prepareStatement(SQL_UPDATE);
			ps.setString(1,c.getNombre());
			ps.setInt(2, c.getId());
			if(ps.executeUpdate()>0){
				return true;
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			con.cerrarConexion();
		}

		return false;
	}

	@Override
	public CicloDTO read(Object key) {
		PreparedStatement ps;
		ResultSet result;
		CicloDTO c=null;

			try {
				ps = con.getCnn().prepareStatement(SQL_READ);
				ps.setInt(1, key.hashCode());
				result=ps.executeQuery();
				while(result.next()){
					c = new CicloDTO(result.getInt(1),result.getString(2));
				}
				return c;
				
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				con.cerrarConexion();
			}

		return c;
	}

	@Override
	public List<CicloDTO> readAll() {
		PreparedStatement ps;
		ResultSet result;
		ArrayList<CicloDTO> ciclos=new ArrayList<>();
		try {
			ps = con.getCnn().prepareStatement(SQL_READALL);
			result=ps.executeQuery();
			while(result.next()){
				ciclos.add (new CicloDTO(result.getInt(1),result.getString(2)));
				}
			return ciclos;
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				con.cerrarConexion();
			}



		return ciclos;
	}

}
