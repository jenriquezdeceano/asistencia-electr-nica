package conexion;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;



public class Conexion {
	public static Conexion instance;
	private Connection cnn;
	
	private Conexion(){
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/adin","root","geneva");
			
		} catch (ClassNotFoundException ex) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//Logger.getLogger(Conexion.class.getName()).log(Level,SERVERE,null,ex);
			// Logger.getLogger(Conexion.class.getName()).log(Level.ALL, null, ex);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			//Logger.getLogger(Conexion.class.getName()).log(Level.ALL,null,ex);
			//e.printStackTrace();
		}
		
	}
	
	public synchronized static Conexion saberEstado(){
		
		if (instance == null){
			instance = new Conexion();
		}
		return instance;
	}
	
	public Connection getCnn(){
		return cnn;
	}
	public void cerrarConexion(){
		try {
			cnn.close();
			instance = null;
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
