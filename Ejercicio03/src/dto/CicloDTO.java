package dto;

public class CicloDTO {


	private int id;
	private String nombre;
	
	public CicloDTO() {
		super();
	}

	public CicloDTO(int id) {
		super();
		this.id = id;
	}
	public CicloDTO(String nombre) {
		super();
		this.nombre = nombre;
	}

	public CicloDTO(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
